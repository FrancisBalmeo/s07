// Learning Objectives
//Create code that will execute only when a given condition has been made

//Topics:
//Assignment Operators 
//Mathematical Operations 
//Conditional Statements
//Ternary Operator

/*

Conditional Statements
- one of the key features of a programming language

Example:
-Is the container full or not?
-Is the temperature higher than or equal to 40 degrees celcius?
-Does the title contain an ampersand?

There are three types of conditional statements:
1. If-Else statement
2. Switch Statement
3. Try-catch-finally statement

Operators
	- allow programming languages to execute operations or evaluations 
	-have different variety of actions within a program

1. Assignment Operator (=)
 	- used to assign a value to a variable

2. Mathematical Operators (+,-,*,/,%)
	- whenever you use a mathematical operator, a value is returned, it is only up to us if we  save that returned value.

	Addition, Subtraction, multiplication, division assignment operators allows us to assign the  result of the operation to the value of the left operand and it allows us to save the result of a mathematical operation to the left operand
*/

//Example of Assignment Operator
let variable = "Initial Value";

//Example of Mathematical Operators
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

/*
	left operand - value of the left side of the operator
	right operand - variable or value of the right side of the operator

	example: sum = num1 + num4 > re-assign value of sum with the result num1 + num4 
*/
let sum = num1 + num4; 
num1 += num4; //Addition Assignment Operator (+=)
console.log(num1);

num2 += num3;
console.log(num2);

num1 += 55;
console.log(num1);
console.log(num4); //previous right should not be affected

let string1 = "Boston";
let string2 = "Celtics";

string1 += string2;
console.log(string1);//BostonCletics - results in concatenation between 2 strings and saves the results in the left operand
console.log(string2);

//15 += num1; produces error because we do not the assignment operator when the left operand is just a value/data

//subtraction assignment operator (-=)

num1 -= num2;
console.log("Result of subtraction assignment operator: " + num1);


//multiplication assignment operator (*=)
num2 *= num3;
console.log("Result of muliplication assignment operator: " + num2);

//division assignment operator (/=)
num4 /= num3;
console.log("Result of division assignment operator: " + num4);

//Arithmetic Operators 
let x = 1397; 
let y = 7831;

let new_sum = x + y;
console.log("Result of addition operator: " + new_sum);

let difference = y - x;
console.log("Result of subtraction operator: " + difference);

let product = x*y;
console.log("Result of multiplication operator: " + product);

let quotient = y/x;
console.log("Result of division operator: " + quotient);

let remainder = y%x;
console.log("Result of modulo operator: " + remainder);

//Multiple Operators and Parenthesis
/*
	- when mulitple operators are applied in a single parenthesis/statement, it follows the PEMDAS (Parentheis, Exponents, Mulitplication, Division, Addition, Subtraction) rule
	- the operators werer done in the following order:
	expression = 1 + 2 - 3 * 4 / 5;
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6

	5. (5 + 3) - 4 = 8

	
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of multiple operations: "+ mdas);

// order of operations can be changed by addin parenthesis
let pemdas = 1 + (2 - 3) * (4 / 5);
/* 
 	-by adding parentheses the order of operations changes priority. It prioritizes operations inside the parentheses first then follow MDAS rule. 

	- the following operation were done in the following order:
	1. 4/5 = 0.8
	2. 2-3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + 0.8 = 0.19

*/
console.log("Result of pemdas operations: "+ pemdas);

let pemdas1 = (1 + (2 - 3)) * (4 / 5);
console.log("Result of 2nd pemdas operations: "+ pemdas1);

//increment and decrement 
/*
	Increment and Decrement 
	- adding or subtracting 1 from the variable and re-assigning the new value to the variable where the increment/decrement was used 
	
	two kinds of incrementation: pre-fix & post-fix
*/
let z = 1;
++z; //prefix increment
console.log("Value of preincrement: " + z);
z++;
console.log("Value of postincrement: "+z);//The value of z was added with 1
console.log(z++); // 3 - with post-incrementation the previous value of the variable is returned first before the actual incrementation was done
console.log(z);// 4 - new value is now returned

//Pre-fix vs Post-fix
console.log(z);
console.log(z++); // prev value returned
console.log(z); // new value now returned

console.log(++z); //new value returned

//predecrement and postdecrement
console.log(--z);
console.log(z--);
console.log(z);

//Comparison Operators 
/*
	-are used to compare values to the left and right operands
	- comparison operators return boolean
		- Equality or Loosely Equality operator (==)
		- Strict Equality Operators (===)
*/

console.log(1 == 1);//true
let isSame = 55 == 55;
console.log(isSame);

console.log(1 == '1'); //loose euqality priority is the sameness of the value because with loose equality operator, forced coercion is done before comparison - JS forcibly changes the data type to the operands

console.log(0 == false);
console.log(1 == true);
console.log("false" == false);
console.log(true == "true");
/*
	with loose comparison operator (==), values are compared and types if operands do not have the same types, it will be forced coerced/type coerced before comparison value

	if either the operand is a number or boolean, the operands are converted into numbers.
*/

console.log(true == "1");// true coerced into a number = 1, "1" is converted into a number = 1 - 1 equals 1

//Strict Equality (===)
console.log(true === "1");// false because it checks both value and type are the same
console.log(1 === '1');//false - both operands have different types
console.log("BTS" === "BTS");//true
console.log("Marie" === "marie");//false

//Inequality and Strict Inequality
// (!=) Inequality Operators 
	/*
	Loose inequality operators
		-checks whether the operands are NOT equal or have different values
		-will do type coercion if the operands have different types
	*/

console.log('1' != 1);
/*
false = both operands were converted to numbers 
*/

console.log("James" != "John");
console.log(1 != true);
//false because with type conversion, true was converted to 1. 1 = 1 so it is not inequal

//Strict Inequality (!==) - checks whether the two operand have different values and will check also if they have different types
console.log("5" !== 5);// true because they have different types
console.log(5 !== 5);// false because operands are equal and have the same type

let name1 = "Jin";
let name2 = "Jimin";
let name3 = "Jungkook";
let name4 = "V";

let number1 = 50; 
let number2 = 60;
let numString1 = "50";
let numString2 = "60";
console.log("")

//Mini-Activity
console.log(numString1 == number1);//true
console.log(numString1 === number1);//false
console.log(numString1 != number1);//false
console.log(name4 !== "num3");//true
console.log(name3 == "Jungkook");//true
console.log(name1 === "Jin");//true

/*
Relational Comparison Operator
	- a comparison operator compares its operands and returns a boolean value based on whether the comparison is true.
*/
console.log("");
let a = 500;
let b = 700; 
let c = 8000;
let numString3 = "5500";

//Greater than (>)
console.log(a > b);
console.log(c > 7);

//less than (<)
console.log(c < a);//false
console.log(b < b);//false
console.log(a < 1000);//true
console.log(numString3 < 1000);//false coercion to schange the string into number
console.log(numString3 < 6000);
console.log(numString3 < "Jose");//true because it is erratic (not irregular or unpredictable)

//Greater than or equal to (>=)
console.log(c >= 10000);
console.log(b >= a);

//Less than or equal to (<=)
console.log(a <= b);
console.log(c <= a);

//Logical Operators
/*
	AND Operator - && 
		- both operands should be true otherwise it results to false

		T && T = T
		F && T = F
		T && F = F
		F && F = F
*/
/*
let isAdmin = false;
let isRegistered = true;
let islegalAge = true;

let authorization = isAdmin && isRegistered; 
console.log(authorization);
let authorization2 = islegalAge && isRegistered;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3);

let authorization4 = isRegistered && islegalAge && requiredLevel === 95;
console.log(authorization4);

let username = "gamer2001";
let username2 = "Shadow1991";
let userage = 15;
let userage2 = 30;


let registration1 = username.length > 8 && userage >= requiredAge;
//.length counts the number of characters
console.log(registration1);// false - userage does not meet the required Age.*/

/*
let registration2 = username2.length > 8 && userage2 >= requiredAge;
console.log(registration2);*/

/*
OR Operator (|| - double pipe)
	- returns true if at least one is true

	T || T = T
	T || F = T
	F || T = T
	F || F = F
*/
/*
let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userage >= requiredAge;
console.log(guildRequirement1);

let guildRequirement2 = isRegistered || userLevel >= requiredLevel || userage >= requiredAge;
console.log(guildRequirement2);


let guildRequirement3 = userLevel >= requiredLevel || userage >= requiredAge;
console.log(guildRequirement3);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

//Not Operator - it turns a boolean into the opposite value

let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin2); //false
console.log(!isRegistered);*/

//Conditional Statements
/*
if-else statements - will run a block of code if the condition specified it met.
*/
/*
console.log("");
let username3 = "crusdader_1993";
let userlevel3 = 25;
let userAge3 = 20;*/

/*if(true){
	alert("We just run an if condition")
}
*/
/*
if (username3.length > 10){
	console.log("Welcome to Game Online!");
}
else{
	console.log("User not found!");
}

if (username3 >= requiredLevel){
	console.log("You are qualified to join the guild!");
}
else{
	console.log("User does not meet qualitfications!");
}*/

// else statement will run if the condition given is false.

/*if (username3.length > 10 && userlevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank You for Joining the Noobies guild!");
}
else{
	console.log("You are too strong to be a Noob! :'(");
}
*/
/*
// else if 
if(username3.length >= 10 && userlevel3 <= 25 && userage >= requiredAge){
	console.log("Thank you for joining the noobies Guild!");
} else if(userlevel3 > 25){
	console.log("You are too strong to be a noob!");
} else if(userAge3 < requiredAge){
	console.log("You are too young to join the guild");
} else if (username3.length < 10){
	console.log("Username too short.")
}*/	

// if else in function
//typeof keyword returns a string which tells the type of data that follows it
/*
function addNum(num1, num2){
	if(typeof num1 === 'number' && typeof num2 === 'number'){
		console.log("run only if both arguments passed are number types");
		console.log(num1 + num2);
	} else{
		console.log("One or both of the arguments are not numbers");
	}
}

addNum(5,1);
addNum("ben",1);

console.log("");
console.log("");
console.log("");*/

	//Mini Activity
		/*
			-add another condition to our nested id statement:
				check if the password is at least 8 characters long
			- add an else statement which will run if both conditions are not met:
			- show an alert "Credential too short"

			Stretch Goals:
				add an else if: if username is less than 8 characters long
					-show an alert "username is too short"
				add an else if: is password less than 8 chatacters long
					-show an alert "password is too short"
		*/

/*
function login(username, password){
	if(typeof username === 'string' && typeof password === 'string'){
		console.log("Both arguments are strings");

		if(password.length >=8 && username.length >=8){
			console.log("Welcome User!");
		}else if (username.length < 8){
			console.log("Username is too short");
		}else if(password.length < 8){
			console.log("Password is too short");
		}
	}else{
		console.log("One of the arguments is not a string");
	}
}

login("user2001", "12345678");
login("user", "12345678");
login("user2002", "1234567");
login(1, "12345");


*/

/*
Mini-Activity
	Create a function which will determine the color shirt depending on the day user inputs

	Monday - Black
	Tuesday - Green
	Wednesday - Yellow
	Thurday - Red
	Friday - Violet
	Saturday - Blue
	Sunday - White

	log a message on alert window:
	"Today is <dayToday>, wear <colorOfTheDay>"

	If the day input is out of range, log message on the alert window:
	"Invalid input. Enter Valid day of the week"

	stretch Goals: 
	check if the argument passed is string:
	log an alert if it is not:
		"Invalid Input. Please input a string"
	Research and use .lowercase()

	if done, send screenshot of your browser and your code in Teams
*/
/*
function colorOfTheDay(day){
	if (typeof day === 'string'){
		if(day.toLowerCase() == "monday"){
			alert(`Today is Monday , Wear Black!`);
		}else if (day.toLowerCase() == "tuesday"){
			alert(`Today is Tuesday, Wear Green!`);
		}else if (day.toLowerCase() == "wednesday"){
			alert(`Today is Wednesday, Wear Yellow!`);
		}else if (day.toLowerCase() == "thursday"){
			alert(`Today is Thursday, Wear Red!`);
		}else if (day.toLowerCase() == "friday"){
			alert(`Today is Friday, Wear Violet!`);
		}else if (day.toLowerCase() == "saturday"){
			alert(`Today is Saturday, Wear Blue!`);
		}else if (day.toLowerCase() == "sunday"){
			alert(`Today is Sunday, Wear White!`);
		}else{
			alert(`Invalid Input: ${day}. Enter a valid day of the week`);
		}
	}else{
		alert('Invalid Input. Please input type String');
	}
}

colorOfTheDay("Monday");
colorOfTheDay("Tuesday");
colorOfTheDay("Wednesday");
colorOfTheDay("Thursday");
colorOfTheDay("Friday");
colorOfTheDay("Saturday");
colorOfTheDay("Sunday");
colorOfTheDay("Francis");
colorOfTheDay(10000);*/

/* Switch Statement
	- an alternative to an if, else-if or else tree. Where the data being evaluated or checked in an expected input
	- if we want to select one of many code blocks/statements to be executed

syntax: 
swtich(expression/condtion){
	case value:
		statement;
		break;
	default:
		statement;
		break;
}
*/
/*
let hero = "Anpanman";

switch(hero){
	case "Jose Rizal":
		console.log("Philippine's National Hero");
		break;
	case "George Washington":
		console.log("Hero of the American Revolution");
		break;
	case "Hercules":
		console.log("Legendary Hero of the Greek");
		break;
	case "Anpanman":
		console.log("Superhero!")
		break;
}*/

/*
function roleChecker(role){
	switch(role){
		case "Admin":
			console.log("Welcome Admin, to the Dashboard.");
			break;
		case "User":
			console.log("You are not authorized to view this page.");
			break;
		case "Guest":
			console.log("Go to the regisration page to register.");
			break;
			//break terminates the code block. If this was not added to your case then, the next case will run as well
		default:
			console.log("Invalid Role");
			//by default your switch ends default case, so therefore, even if there is no break keyword in your default case, it will not run anything else
	}
}
roleChecker("Admin");
roleChecker("User");
roleChecker("Guest");
roleChecker("Abba");
*/

/* Mini Activity 

create a colorOfTheDay function, instead of using if-else, convert it to a switch statement(use switch statement)

*/

/*

function colorOfTheDay(day){
	switch(day.toLowerCase()){
		case "monday":
		alert(`Today is ${day} , Wear Black!`);
		break;
		case "tuesday":
		alert(`Today is ${day} , Wear Green!`);
		break;
		case "wednesday":
		alert(`Today is ${day} , Wear Yellow!`);
		break;
		case "thursday":
		alert(`Today is ${day} , Wear Red!`);
		break;
		case "friday":
		alert(`Today is ${day} , Wear Purple!`);
		break;
		case "saturday":
		alert(`Today is ${day} , Wear Blue!`);
		break;
		case "sunday":
		alert(`Today is ${day} , Wear White!`);
		break;
		default:
		alert(`Inalid Input: ${day}. Please enter a valid day of the week.`);
		break;
	}
}

colorOfTheDay("Monday");
colorOfTheDay("Tuesday");
colorOfTheDay("Francis");*/

//function with if-else return

function gradeEvaluator(grade){
	/*

	evaluate the grade input and return the letter distinction
		-if grade is less than or equal to 70 = F
		- if the grade is greater than or equal to 71 = C
		- if grade is greater than 80 = B
		- if grade is greater than or equal 90 = A
	*/
	let score;
		switch(true){
		
		case (grade <= 70):
			score = "F";
			break;
		case (grade>=71 && grade<=79):
			score ="C";
			break;
		case (grade>=80 && grade <=89):
			score = "B";
			break;
		case (grade>=90 && grade<100):
			score = "A";
			break;
		default:
			return "Enter a number between 0 and 100";
		}
		return score;
}

console.log(gradeEvaluator(70));
console.log(gradeEvaluator(71));
console.log(gradeEvaluator(80));
console.log(gradeEvaluator(90));


/* Ternary Operators

shorthand way of writing if-else statements

syntax:
conditiion ? if-statement: else statement
*/

let price = 5000;
price > 1000 ? console.log("Price is over 1000"):console.log("Price is less than 1000")

let hero1 = "Goku";
hero1 === "Vegeta" ? console.log("Prince of all Saiyan"):console.log("You are not even royalty")

let villain = "Harvey Dent";
villain === "Two Faces"
? console.log("You live long enough to be a villain")
:console.log("Not quite villainous yet.")


let robin = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin
? true
: false
console.log(isFirstRobin);
/*
let number = 7;
number === 5;
? console.log("A")
: (number === 10 ? console.log("A is 10") :console.log("A is not 5 or 10"))*/

