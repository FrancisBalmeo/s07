//Date 05/25/2022
function oddEvenChecker(number){
 if(typeof number === 'number'){
 	if(number%2 == 0){
		console.log("The number is even.");
	}else{
		console.log("The number is odd.");
	}
 }else{
 	alert("Invalid Input");
 }
}

oddEvenChecker(20);
oddEvenChecker(45);
oddEvenChecker("Francis");

function budgetChecker(budget){
	if(typeof budget === 'number'){
		budget > 40000
		?console.log("You are over the budget")
		:console.log("You have resources left")
	}else{
		alert("Invalid Input");
	}
}

budgetChecker(30000);
budgetChecker(45000);
budgetChecker("Balmeo");